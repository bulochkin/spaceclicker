package org.vasyabulochkin.spaceclicker;

import com.badlogic.gdx.*;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.scenes.scene2d.*;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.*;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import java.text.*;
import java.util.*;

import java.util.List;
import java.math.*;

public class MyGdxGame implements ApplicationListener, InputProcessor
{

	@Override
	public boolean keyDown(int p1)
	{
		// TODO: Implement this method
		return false;
	}

	@Override
	public boolean keyUp(int p1)
	{
		// TODO: Implement this method
		return false;
	}

	@Override
	public boolean keyTyped(char p1)
	{
		// TODO: Implement this method
		return false;
	}

	@Override
	public boolean touchDown(int p1, int p2, int p3, int p4)
	{
        if (p3 == 0)
		    makeDonut(p1, p2, true);
		return false;
	}

	@Override
	public boolean touchUp(int p1, int p2, int p3, int p4)
	{
		// TODO: Implement this method
		return false;
	}

	@Override
	public boolean touchDragged(int p1, int p2, int p3)
	{
		//makeDonut(p1, p2, true);
		return false;
	}

	@Override
	public boolean mouseMoved(int p1, int p2)
	{
		// TODO: Implement this method
		return false;
	}

	@Override
	public boolean scrolled(int p1)
	{
		// TODO: Implement this method
		return false;
	}

	Texture texture, tex, texFull, texGold, texZero;
	SpriteBatch batch;
	List<Donut> donuts;
	float lastCreateDelta=0;
	BigInteger score = new BigInteger("0");
	BigInteger scoreInc = new BigInteger("1");
	BitmapFont fnt;
	Skin skin;
	Stage stage;
	BigInteger price = new BigInteger("100");
	BigInteger priceAutoInc = new BigInteger("500");
	BigInteger priceAutoDelta = new BigInteger("1000");
	float lastAuto = 0;
	float autoDelta = 3;
	BigInteger autoInc = new BigInteger("0");
	Random rnd = new Random();
    List<Label> textFx;

	private void Load()
	{
		Preferences prefs = Gdx.app.getPreferences("score");
		score = new BigInteger(prefs.getString("score", score.toString()));
		scoreInc = new BigInteger(prefs.getString("inc", scoreInc.toString()));
		autoInc = new BigInteger(prefs.getString("autoInc", autoInc.toString()));
		autoDelta = prefs.getFloat("autoDelta", autoDelta);
		price = new BigInteger(prefs.getString("price", price.toString()));
		priceAutoInc = new BigInteger(prefs.getString("priceAutoInc", priceAutoInc.toString()));
		priceAutoDelta = new BigInteger(prefs.getString("priceAutoDelta", priceAutoDelta.toString()));
	}

	private void Save()
	{
		Preferences prefs = Gdx.app.getPreferences("score");
		prefs.putString("score", score.toString());
		prefs.putString("inc", scoreInc.toString());
		prefs.putString("autoInc", autoInc.toString());
		prefs.putFloat("autoDelta", autoDelta);
		prefs.putString("price", price.toString());
		prefs.putString("priceAutoInc", priceAutoInc.toString());
		prefs.putString("priceAutoDelta", priceAutoDelta.toString());
		prefs.flush();
	}

    private void makeButtons()
    {
        VerticalGroup menu = new VerticalGroup();
        menu.left();
        menu.setWidth(310);
        menu.setHeight(Gdx.graphics.getHeight());
        menu.space(10);
        menu.padLeft(10);
        menu.padTop(10);
        //***************************************
        Table tbl = new Table();
        tbl.setSize(300, 100);
        tbl.setBackground(skin.newDrawable("buttBckgInc"));
        final TextButton button = new TextButton(price.toString(), skin);
        final Label lbl = new Label(scoreInc.toString(), skin);
        button.addListener(new ChangeListener() {
            public void changed (ChangeEvent event, Actor actor) {
                if (score.compareTo(price) >= 0)
                {
                    score = score.subtract(price);
                    price = price.multiply(new BigInteger("3"));
                    scoreInc = scoreInc.multiply(new BigInteger("2"));
                    button.setText(price.toString());
                    lbl.setText(scoreInc.toString());
                }
                event.cancel();
            }
        });
        tbl.add(lbl).left().bottom().expand().pad(5);
        tbl.add(button).right().pad(10).expand().bottom();
        menu.addActor(tbl);
        //***************************************
        final TextButton button2 = new TextButton(priceAutoInc.toString(), skin);
        final Label lbl2 = new Label(autoInc.toString(), skin);
        button2.addListener(new ChangeListener() {
            public void changed (ChangeEvent event, Actor actor) {
                if (score.compareTo(priceAutoInc) >= 0)
                {
                    score = score.subtract(priceAutoInc);
                    priceAutoInc = priceAutoInc.multiply(new BigInteger("3"));
                    if (autoInc.compareTo(BigInteger.ZERO) == 0)
                        autoInc = new BigInteger("1");
                    else
                        autoInc = autoInc.multiply(new BigInteger("2"));
                    button2.setText(priceAutoInc.toString());
                    lbl2.setText(autoInc.toString());
                }
                event.cancel();
            }
        });
        tbl = new Table();
        tbl.setSize(300, 100);
        tbl.setBackground(skin.newDrawable("buttBckgAutoInc"));
        tbl.add(lbl2).left().bottom().expand().pad(5);
        tbl.add(button2).right().pad(10).expand().bottom();
        menu.addActor(tbl);
        //***************************************
        final TextButton button3 = new TextButton(priceAutoDelta.toString(), skin);
        final Label lbl3 = new Label(new DecimalFormat("#.##").format(autoDelta), skin);
        button3.addListener(new ChangeListener() {
            public void changed (ChangeEvent event, Actor actor) {
                if (score.compareTo(priceAutoDelta) >= 0)
                {
                    score = score.subtract(priceAutoDelta);
                    priceAutoDelta = priceAutoDelta.multiply(new BigInteger("3"));
                    autoDelta *= 0.8;
                    button3.setText(priceAutoDelta.toString());
                    lbl3.setText(new DecimalFormat("#.##").format(autoDelta));
                }
                event.cancel();
            }
        });
        tbl = new Table();
        tbl.setSize(300, 100);
        tbl.setBackground(skin.newDrawable("buttBckgAutoDelta"));
        tbl.add(lbl3).left().bottom().expand().pad(5);
        tbl.add(button3).right().pad(10).expand().bottom();
        //***************************************

        menu.addActor(tbl);
        menu.setDebug(true, true);
        stage.addActor(menu);
    }

    private void setupSkin()
    {
        texture = new Texture(Gdx.files.internal("space.png"));
        texZero = new Texture(Gdx.files.internal("0.png"));
        tex = new Texture(Gdx.files.internal("1.png"));
        texFull = new Texture(Gdx.files.internal("2.png"));
        texGold = new Texture(Gdx.files.internal("3.png"));

        fnt = new BitmapFont();
        fnt.setColor(Color.valueOf("6ae2bc"));

        skin = new Skin();

        Pixmap pixmap = new Pixmap(1, 1, Format.RGBA8888);
        pixmap.setColor(Color.WHITE);
        pixmap.fill();
        skin.add("white", new Texture(pixmap));
        skin.add("bckg", texture);
        skin.add("buttBckg", new Texture(Gdx.files.internal("buttBckg.png")));
        skin.add("buttBckgInc", new Texture(Gdx.files.internal("buttBckgInc.png")));
        skin.add("buttBckgAutoInc", new Texture(Gdx.files.internal("buttBckgAutoInc.png")));
        skin.add("buttBckgAutoDelta", new Texture(Gdx.files.internal("buttBckgAutoDelta.png")));

        skin.add("default", fnt);

        TextButtonStyle textButtonStyle = new TextButtonStyle();
        textButtonStyle.up = skin.newDrawable("white", Color.RED);
        textButtonStyle.down = skin.newDrawable("white", Color.DARK_GRAY);
        textButtonStyle.checked = skin.newDrawable("white", Color.GRAY);
        textButtonStyle.over = skin.newDrawable("white", Color.LIGHT_GRAY);
        textButtonStyle.font = skin.getFont("default");
        textButtonStyle.font.getData().setScale(2);
        skin.add("default", textButtonStyle);

        Label.LabelStyle lblStyle = new Label.LabelStyle();
        lblStyle.font = skin.getFont("default");
        lblStyle.font.getData().setScale(2);
        lblStyle.background = skin.newDrawable("white", Color.RED);
        skin.add("default", lblStyle);
        Label.LabelStyle lblStyleTransparent = new Label.LabelStyle();
        lblStyleTransparent.font = skin.getFont("default");
        lblStyleTransparent.font.getData().setScale(2);
        lblStyleTransparent.fontColor = Color.GOLDENROD;
        skin.add("transparent", lblStyleTransparent);
    }

	@Override
	public void create()
	{
		Load();
		donuts = new ArrayList<Donut>();
		batch = new SpriteBatch();
        textFx = new ArrayList<Label>();
		stage = new Stage();

        setupSkin();

        makeButtons();

		InputMultiplexer mult = new InputMultiplexer();
		mult.addProcessor(stage);
		mult.addProcessor(this);
		Gdx.input.setInputProcessor(mult);
	}

	@Override
	public void render()
	{
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		batch.begin();

		batch.draw(texture, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		for (Iterator<Donut> i = donuts.iterator(); i.hasNext();)
		{
			i.next().spr.draw(batch);
		}
		fnt.draw(batch, String.valueOf(score), 50,50);

		batch.end();
		stage.act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));
		stage.draw();

		for (int i = 0; i < donuts.size(); ++i)
		{
			donuts.get(i).update(Gdx.graphics.getDeltaTime());
			if (donuts.get(i).isDead())
			{
				donuts.remove(i);
				i--;
			}
		}
		lastCreateDelta+=Gdx.graphics.getDeltaTime();
		lastAuto+=Gdx.graphics.getDeltaTime();
		if (autoInc.compareTo(BigInteger.ZERO) > 0 && lastAuto >= autoDelta)
		{
			makeDonut(rnd.nextInt(Gdx.graphics.getWidth()), rnd.nextInt(Gdx.graphics.getHeight()), false);
		}
        for (int i = 0; i < textFx.size(); ++i)
        {
            if (textFx.get(i).getActions().size == 0)
            {
                textFx.get(i).remove();
                textFx.remove(i);
                i--;
            }
        }
	}

	@Override
	public void dispose()
	{

	}

	@Override
	public void resize(int width, int height)
	{
		stage.getViewport().update(width, height, true);
	}

	@Override
	public void pause()
	{
		Save();
	}

	@Override
	public void resume()
	{

	}

	private boolean makeDonut(int x, int y, boolean isManual)
	{
		BigInteger coef = BigInteger.ONE;
        BigInteger scoreDiff ;
		int r = rnd.nextInt(1000);
        if (r <= 3)
        {
            donuts.add(new Donut(texZero, 7));
        } else if (r <= 10)
		{
			donuts.add(new Donut(texGold, 7));
			coef = BigInteger.TEN;
		} else if (r <= 30)
		{
			donuts.add(new Donut(texFull, 5));
			coef = new BigInteger("3");
		} else
		{
			donuts.add(new Donut(tex, 3));
		}
		if (isManual)
		{
			lastCreateDelta = 0;
			scoreDiff = r>3?scoreInc.multiply(coef):scoreInc.divide(new BigInteger("2"));
		} else {
            scoreDiff = r>3?autoInc.multiply(coef):autoInc.divide(new BigInteger("2"));
			lastAuto = 0;
		}
        score = score.add(scoreDiff);
		donuts.get(donuts.size() - 1).spr.setPosition(x - tex.getWidth() / 2, Gdx.graphics.getHeight() - (y + tex.getHeight() / 2));
        if (textFx.size() > 30)
        {
            textFx.get(0).remove();
            textFx.remove(0);
        }

        Label lbl = new Label(scoreDiff.toString(), skin, "transparent");
        lbl.setPosition(50, 50);
        lbl.addAction(Actions.parallel(Actions.moveTo(50, 300, 3), Actions.alpha(0, 3)));
        textFx.add(lbl);
        stage.addActor(lbl);
		return true;
	}
}
