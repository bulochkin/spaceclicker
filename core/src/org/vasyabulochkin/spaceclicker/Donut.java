package org.vasyabulochkin.spaceclicker;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g2d.*;

public class Donut
{
	private float life;
	public Sprite spr;
	public Donut(Texture tex, float lifetime)
	{
		spr = new Sprite(tex);
		life = lifetime;
	}
	
	public void update(float delta)
	{
		life -= delta;
	}
	
	public boolean isDead()
	{
		return life <= 0;
	}
}
