package org.vasyabulochkin.spaceclicker;
import java.math.*;

public class GameState
{
	float lastCreateDelta=0;
	BigInteger score = new BigInteger("0");
	BigInteger scoreInc = new BigInteger("1");
	BigInteger price = new BigInteger("100");
	BigInteger priceAutoInc = new BigInteger("500");
	BigInteger priceAutoDelta = new BigInteger("1000");
	float lastAuto = 0;
	float autoDelta = 3;
	BigInteger autoInc = new BigInteger("0");
}
